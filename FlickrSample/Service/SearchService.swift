//
//  SearchService.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation
class SearchService: Service {
    
    private let networkRepo: SearchRepository
    
    required init() {
        networkRepo = SearchRepository()
    }
    
    func getRequest(searchText: String, page: Int, perPage: Int,  completion: @escaping GetSearchResponse) {
        let parameters = "&text=\(searchText)&page=\(page)&per_page=\(perPage)"

        networkRepo.getRequest(parameters: parameters, completion: completion)
    }
}

