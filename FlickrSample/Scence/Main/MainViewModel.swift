//
//  MainViewModel.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation

class MainViewModel {

    private var service: SearchService = SearchService()
    var dataSource: CollectionDataSource<Photo>!
    
//    var photosSearchText: String = ""
    var keyword: String = ""

    let itemsPerPage = 30
    var currentPage = 0
    var totalPages = 0
    var error = DynamicValue<String?>(nil)
    var isLoading = DynamicValue<Bool?>(nil)
    
    var updateCollectionView: ((_ beginingIndex: Int, _ newItemsCount: Int) -> Void)?
    


    init(dataSource: CollectionDataSource<Photo>) {
        self.dataSource = dataSource
    }
    
    
    // MARK: - Data Interaction

    private func getPhoto(completion: @escaping ([Photo]) -> Void) {
        service.getRequest(searchText: keyword, page: currentPage, perPage: itemsPerPage) { result in
            switch result {
            case .success(let result):
                if result.stat != "ok" {
                    self.error.value = result.message
                    completion([])
                }else {
                    self.totalPages = result.photos!.pages
                    completion(result.photos!.photo)
                }
               
            case .failure(let error):
                
                self.error.value = error.localizedDescription
            }
        }
        
    }
    
    func searchBarDidChange(_ text: String) {
        keyword = text
        currentPage = 0
        dataSource?.data.value = []
        dataSource?.isLoading = false
        isLoading.value = false
        fetchData()
    }
    
    
    func fetchData() {
        currentPage = 0
        dataSource?.isLoading = true
        isLoading.value = true
        
        getPhoto { [weak self] result in
            
            guard let `self` = self else { return }
            let lastItemIndex = max((self.dataSource!.data.value.count), 0)

            self.dataSource?.data.value = result
            self.dataSource?.isLoading = false
            self.isLoading.value = false
            self.updateCollectionView?(lastItemIndex,result.count)


        }

    }

    
    func requestNextPhotosPage() {
        
        if dataSource?.isLoading == false, totalPages > currentPage {
            dataSource?.isLoading = true
            currentPage += 1
            getPhoto { [weak self] data in
                let lastItemIndex = max(((self?.dataSource!.data.value.count)!), 0)

                self?.dataSource?.data.value += data
                self?.dataSource?.isLoading = false
                self?.updateCollectionView?(lastItemIndex,data.count)

            }

        }

        

    }


}

