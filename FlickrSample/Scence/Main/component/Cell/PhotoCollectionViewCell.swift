//
//  PhotoCollectionViewCell.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation
import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    let posterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 2
        imageView.backgroundColor = .gray
        return imageView
    }()
    
  
    
    private let imagePadding: CGFloat = 6
    private let borderViewPadding: CGFloat = 3
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [], animations: {
                self.transform = self.isHighlighted ? CGAffineTransform(scaleX: 0.95, y: 0.95) : .identity
            }, completion: nil)
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpLayout()
        setUpViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    private func setUpViews() {
        posterImageView.layer.cornerRadius = ( bounds.width - (imagePadding * 2) ) / 2
    }
    
    private func setUpLayout() {
        
        contentView.addSubview(posterImageView)
        
        
        posterImageView.anchor(top: contentView.topAnchor,
                                 leading: contentView.leadingAnchor,
                                 bottom: nil, trailing: contentView.trailingAnchor,
                                 padding: .init(top: imagePadding,
                                                left: imagePadding,
                                                bottom: 0,
                                                right: imagePadding))
        posterImageView.heightAnchor.constraint(equalTo: posterImageView.widthAnchor).isActive = true
        
        
    }
    
    public func fillCell(with data: Photo) {
        self.posterImageView.setRemoteImage(path: data.imageURL())
    }
    
}
