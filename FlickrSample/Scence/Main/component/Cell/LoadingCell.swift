//
//  LoadingCell.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//


import UIKit

class LoadingCell: UICollectionReusableView {

    static var id: String {
        return String(describing: self)
    }

    static var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.color = .systemPink
        spinner.tintColor = .systemPink
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        return spinner
    }()

    override func awakeFromNib() {
        self.addSubview(LoadingCell.spinner)
        LoadingCell.spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        LoadingCell.spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(LoadingCell.spinner)
        LoadingCell.spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        LoadingCell.spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
