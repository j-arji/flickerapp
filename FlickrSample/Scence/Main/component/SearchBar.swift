//
//  SearchBar.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation
import UIKit

class SearchBarView: UIView {
    
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.placeholder = "Search"
        searchBar.barStyle = .default
        return searchBar
    }()
    
    weak var searchBarDelegate: UISearchBarDelegate?
    private weak var timer: Timer?

    
    private var textFieldInsideSearchBar: UITextField? {
        if #available(iOS 13.0, *) {
            return searchBar.searchTextField
        } else {
            return searchBar.value(forKey: "searchField") as? UITextField
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(searchBar)
        // search bar has 8px padding from right and left 12 + 8 = 20
        searchBar.fillSuperview(padding: .init(top: 0, left: 12, bottom: 0, right: 12))
        setUpViews()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    
    private func setUpViews() {
        guard let searchBarTextField = textFieldInsideSearchBar else {return}

        searchBarTextField.backgroundColor = UIColor(named: "quaternaryTextColor")
        if let searchIcon = searchBarTextField.leftView as? UIImageView {
            searchIcon.tintColor = .gray
        }
        searchBarTextField.layer.cornerRadius = 16
        searchBarTextField.layer.masksToBounds = true
        searchBarTextField.textColor = .white
        searchBarTextField.returnKeyType = .done
        backgroundColor = .black
        searchBar.tintColor = .gray
    }
     
}

extension SearchBarView: UISearchBarDelegate {
    
    // MARK: - searchbar Delegate functions
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBarDelegate?.searchBarSearchButtonClicked?(searchBar)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBarDelegate?.searchBarCancelButtonClicked?(searchBar)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false, block: { [weak self] (_) in
            self?.searchBarDelegate?.searchBar?(searchBar, textDidChange: searchText)
        })
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBarDelegate?.searchBarTextDidBeginEditing?(searchBar)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBarDelegate?.searchBarTextDidEndEditing?(searchBar)
    }
}
