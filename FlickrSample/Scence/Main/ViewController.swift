//
//  ViewController.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import UIKit

class ViewController: UIViewController {

    
    private var dataSource = MainCollectionDataSource()

    
    lazy var viewModel: MainViewModel = {
        let viewModel = MainViewModel(dataSource: dataSource)
        return viewModel
    }()
    private lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: .medium)
        spinner.color = .red
        spinner.tintColor = .red
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        spinner.alpha = 1
        return spinner
    }()
    private let collectionLayout = UICollectionViewFlowLayout()
    
    private lazy var collectionView: UICollectionView = {
        let collectionview = UICollectionView(frame: view.frame, collectionViewLayout: collectionLayout)
        collectionview.dataSource = dataSource
        collectionview.delegate = self
        collectionview.allowsMultipleSelection = false
        collectionview.keyboardDismissMode = .onDrag
        collectionview.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: PhotoCollectionViewCell.identifier)
        collectionview.register(LoadingCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: LoadingCell.id)
        return collectionview
    }()
    
    var selectedCell: PhotoCollectionViewCell?
    var selectedCellImageViewSnapshot: UIView?
    var animator: Animator?

    private let searchBarView = SearchBarView()
    
    // MARK: - Constants
    private let leftRightPadding: CGFloat = 22
    private let topBottomPadding: CGFloat = 16
    private let innerSpacing: CGFloat = 32
    private let numberOfItemsInARow: CGFloat = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        setupSearchBar()
        setupCollection()
        bindErrors()
        bindCollectionView()
    }
    
    
    func bindCollectionView() {
    
        viewModel.updateCollectionView = { [weak self] startIndex,newItemsCount in
            var indexPathstoInsert: [IndexPath] = []
            for loopIndex in 0..<newItemsCount {
                indexPathstoInsert.append( IndexPath(item: startIndex + loopIndex, section: 0))
            }
            self?.collectionView.performBatchUpdates {
                self?.collectionView.insertItems(at: indexPathstoInsert)
            }completion: { (_) in}
        }
    }
    
    private func bindIndicatorView() {
        viewModel.isLoading.addAndNotify(observer: self) { [weak self] value in
            guard let isHidden = value else { return }
            if isHidden {
                self?.spinner.startAnimating()
            }else {
                self?.spinner.stopAnimating()
            }
        }
    }
    private func bindErrors() {
        viewModel.error.addAndNotify(observer: self) { [weak self] (value) in
            guard let message = value else { return }
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
    

    func setupSearchBar() {
        view.addSubview(searchBarView)
        view.addSubview(collectionView)
        searchBarView.searchBarDelegate = self

        searchBarView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                             leading: view.leadingAnchor,
                             bottom: collectionView.topAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: 12, bottom: 8, right: 12),
                             size: .init(width: 0, height: 66))
    }
    
    
    private func setupCollection() {
        collectionView.anchor(top: nil,
                              leading: view.leadingAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              trailing: view.trailingAnchor,
                              padding: .init(top: 0, left: 0, bottom: 0, right: 0))
    }
    
    
    func ShowPhoto(with data: Photo) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            // B1 - 4
        detailVC.transitioningDelegate = self

        detailVC.modalPresentationStyle = .fullScreen
        detailVC.data = data
        present(detailVC, animated: true)
    }
}

extension ViewController: UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchBarDidChange(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}


extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCell = collectionView.cellForItem(at: indexPath) as? PhotoCollectionViewCell
        
        selectedCellImageViewSnapshot = selectedCell?.posterImageView.snapshotView(afterScreenUpdates: false)
        ShowPhoto(with: dataSource.data.value[indexPath.row])
    }
}


// MARK: - collectionViewFlowlayout  functinos
extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.safeAreaLayoutGuide.layoutFrame.width
        let paddedWidth = totalWidth - CGFloat(leftRightPadding * 2 )
        let innerSpaceWidth = (numberOfItemsInARow - 1) * innerSpacing
        let widthOfEachCell = ( paddedWidth - innerSpaceWidth) / numberOfItemsInARow
        //30 is the extra hieght for subtitle
        return .init(width: widthOfEachCell, height:  widthOfEachCell + 35  )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: topBottomPadding, left: leftRightPadding, bottom: topBottomPadding, right: leftRightPadding )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return innerSpacing / 2
    }
}


// MARK: - collectionView header functions
extension ViewController {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return dataSource.isLoading ? CGSize(width: collectionView.safeAreaLayoutGuide.layoutFrame.width, height: 40) :.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionView.elementKindSectionFooter {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadingCell.id, for: indexPath) as! LoadingCell
            return footerView
        }
        return UICollectionReusableView()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (collectionView.contentSize.height > collectionView.frame.size.height && dataSource.data.value.count - 1 == indexPath.item) {
            viewModel.requestNextPhotosPage()
        }
    }

}




extension ViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let firstViewController = presenting as? ViewController,
            let secondViewController = presented as? DetailViewController,
            let selectedCellImageViewSnapshot = selectedCellImageViewSnapshot
            else { return nil }

        animator = Animator(type: .present, firstViewController: firstViewController, secondViewController: secondViewController, selectedCellImageViewSnapshot: selectedCellImageViewSnapshot)
        return animator
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let secondViewController = dismissed as? DetailViewController,
            let selectedCellImageViewSnapshot = selectedCellImageViewSnapshot
            else { return nil }

        animator = Animator(type: .dismiss, firstViewController: self, secondViewController: secondViewController, selectedCellImageViewSnapshot: selectedCellImageViewSnapshot)
        return animator
    }
}
