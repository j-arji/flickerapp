//
//  MainCollectionDataSource.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import UIKit

class MainCollectionDataSource: CollectionDataSource<Photo>, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  data.value.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
        let item =  data.value[indexPath.row]
        cell.fillCell(with: item)
        return cell
    }
    
    
}
