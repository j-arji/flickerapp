//
//  DetailViewController.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var data: Photo!
    
    @IBOutlet private(set) var closeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let cross = #imageLiteral(resourceName: "cross")
        closeButton.setImage(cross, for: .normal)
        closeButton.tintColor = .white

        imageView.setRemoteImage(path: data.imageURL())
    }

    @IBAction func close(_ sender: Any) {
        dismiss(animated: true)
    }
}

