//
//  NetworkRouter.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation
import Alamofire

enum RequestMethod: String {
    case get, post, put, patch, trace, delete
}

protocol NetworkRouter: URLRequestConvertible {
    var baseURLString :String? { get }
    var method: RequestMethod? { get }
    var apiKey: String { get }
    var path: String {get }
    var headers: [String: String]? { get }
    var params: [String: Any]? { get }
    func asURLRequest() throws -> URLRequest
}

extension NetworkRouter {
    
//    typealias ResultRouter<T: Codable> = Result<T, Error>
    
    var baseURLString: String? {
        return "https://api.flickr.com/services/rest/"

    }
    
    // Add Rout method here
    var method: RequestMethod? {
        return .post
    }
    var apiKey: String {
        ApiConstants.shared.apiKey
    }
    
    // Set APIs'Rout for each case
    var path: String {
        return ""
    }
    
    // Set header here
    var headers: [String: String]? {
        return [:]
    }
    
    // Set encoding for each APIs
    var encoding: ParameterEncoding? {
        return JSONEncoding.default
    }
    
    // Return each case parameters
    var params: [String: Any]? {
        return [:]
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: self.baseURLString!.appending(path))
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = method?.rawValue.uppercased()
        urlRequest.allHTTPHeaderFields = headers
        urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        if self.method == .get {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: self.params)
        } else {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: self.params ?? [:], options: .prettyPrinted)
        }
        return urlRequest
    }
}
