//
//  DataSource.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation
import UIKit

protocol DataSource: class {
    associatedtype Data
    var data: DynamicValue<[Data]> { get set }
}

protocol Pageable {
    var isLoading: Bool { get set }
}

// - TableView Section Pattern

protocol Section {
    var title: String? { get }
    var items: [SectionItem] { get }
}

protocol SectionItem {
    var identifier: String { get }
    func cell(from tableView: UITableView) -> UITableViewCell
}



class CollectionDataSource<T>: NSObject, DataSource, Pageable {
    typealias Data = T
    var data: DynamicValue<[Data]> = DynamicValue<[Data]>([])
    var isLoading: Bool = false
}


//protocol SectionCollectionItem {
//    var identifier: String { get }
//    func cell(from tableView: UICollectionView) -> UICollectionViewCell
//}




