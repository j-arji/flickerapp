//
//  SearchRepository.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation
typealias GetSearchResponse = ((Result<Photos, ClientError>) -> ())


struct SearchRepository {
    private let client: APIClient
    
    init() {
        self.client = DependencyContainer.resolve()
    }
    // MARK: - API Requests

    
    
    func getRequest(parameters: String, completion: @escaping GetSearchResponse) {
        let router = Router.getRequest(parameters: parameters)
        client.request(router) { (response: Result<Photos, ClientError>) -> (Void) in
            completion(response)
        }
    }
    
}

extension SearchRepository {
    enum Router: NetworkRouter {
        case getRequest(parameters: String)
        
        var method: RequestMethod? {
            switch self {
            case .getRequest:
                return .get
            }
        }
        
        var path: String {
            switch self {
            case .getRequest(let parameters):
                return "?method=flickr.photos.search&format=json&nojsoncallback=1\(parameters)&api_key=\(apiKey)"
                
            }
        }
    }
}

