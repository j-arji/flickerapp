//
//  UIImageView+SetRemotes.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation
import Kingfisher

extension UIImageView {

    func setRemoteImage(url: URL?) {
        DispatchQueue.main.async {
            self.kf.setImage(with: url, options: [.transition(ImageTransition.fade(0.2))])
        }
    }

    func setRemoteImage(path: String?, placeholder: UIImage? = nil) {
        guard let path = path, let url = URL(string: path) else {
            image = placeholder
            return
        }
        DispatchQueue.main.async {
            self.kf.indicatorType = .activity
            self.kf.setImage(with: url, placeholder: placeholder, options: [.transition(ImageTransition.fade(0.2))])

        }

    }

}
