//
//  UICollectionReusableView + Extention.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import UIKit

extension UICollectionReusableView {

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
