//
//  Photo.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation

class Photo: Codable {
    var id: String
    var owner: String
   
    private let farm: Int
    private let server: String
    private let secret: String
    private let title: String

}


extension Photo {

  func imageURL() -> String {
    return "https://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg"
  }

}

