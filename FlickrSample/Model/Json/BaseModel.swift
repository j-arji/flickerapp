//
//  BaseModel.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation

class BaseModel: Codable {
    var page: Int
    var pages: Int
    var perpage: Int
    var total: String
    var photo: [Photo]
    
}

class Photos: Codable {
    var  photos: BaseModel?
    var stat: String
    var code: Int?
    var message: String?
}

