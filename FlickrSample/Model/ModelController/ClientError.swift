//
//  ClientError.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//


import Foundation

enum ClientError: Int, Error {
    
    typealias RawValue = Int
    
    case unreachable = 1000
    case timeOut = 504
    case unprocessable = 422
//    case unauthentication
    case internalServer = 500
    case parser = 1001
    case unknown = 1002
    case noContent = 204
    case notFound = 404
    case unauthorized = 401
    case badRequest = 400
    case forbidden = 403
    case methodNotAllowed = 405
    case conflict = 409
    case tooManyRequests = 429
    case legalReasons = 451
    case badGateway = 502
    case invalidRequest = 509
}

extension ClientError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .unreachable:
            return "error_network_unreachable_message"
        case .timeOut, .internalServer:
            return "error_server_unreachable_message"
        case .unprocessable:
            return "unprocceessable entity"
        case .parser:
            return "Parser error."
        case .unknown:
            return "Unknown error."
        case .noContent:
            return "No Content"
        case .notFound:
            return "Not found"
        case .unauthorized:
            return "Unauthorized Access"
        case .badRequest:
            return "Invalid Request Parameters"
        case .forbidden:
            return "Forbidden Access"
        case .methodNotAllowed:
            return "Method Not Allowed"
        case .conflict:
            return "Conflict"
        case .tooManyRequests:
            return "Too many requests"
        case .legalReasons:
            return "Deny access in order to Legal reasons"
        case .badGateway:
            return "Bad gateway"
        case .invalidRequest:
            return "Invalid Request parameters"
        }
    }
}


//1: Too many tags in ALL query
//When performing an 'all tags' search, you may not specify more than 20 tags to join together.
//2: Unknown user
//A user_id was passed which did not match a valid flickr user.
//3: Parameterless searches have been disabled
//To perform a search with no parameters (to get the latest public photos, please use flickr.photos.getRecent instead).
//4: You don't have permission to view this pool
//The logged in user (if any) does not have permission to view the pool for this group.
//5: User deleted
//The user id passed did not match a Flickr user.
//10: Sorry, the Flickr search API is not currently available.
//The Flickr API search databases are temporarily unavailable.
//11: No valid machine tags
//The query styntax for the machine_tags argument did not validate.
//12: Exceeded maximum allowable machine tags
//The maximum number of machine tags in a single query was exceeded.
//17: You can only search within your own contacts
//The call tried to use the contacts parameter with no user ID or a user ID other than that of the authenticated user.
//18: Illogical arguments
//The request contained contradictory arguments.
//100: Invalid API Key
//The API key passed was not valid or has expired.
//105: Service currently unavailable
//The requested service is temporarily unavailable.
//106: Write operation failed
//The requested operation failed due to a temporary issue.
//111: Format "xxx" not found
//The requested response format was not found.
//112: Method "xxx" not found
//The requested method was not found.
//114: Invalid SOAP envelope
//The SOAP envelope send in the request could not be parsed.
//115: Invalid XML-RPC Method Call
//The XML-RPC request document could not be parsed.
//116: Bad URL found
//One or more arguments contained a URL that has been used for abuse on Flickr.
