//
//  KeychainManager.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation

struct KeychainManager {
    
    private enum Constant: String {
        case access = "accessToken"
        case refresh = "refreshToken"
    }
    
    static var shared = KeychainManager()
    
    func getTokenFromKeychain() -> Token? {
        let keychainItemWrapper = KeychainItemWrapper()
        if let accessToken = keychainItemWrapper[Constant.access.rawValue] as? String,
           let refreshToken = keychainItemWrapper[Constant.refresh.rawValue] as? String {
            let token = Token(accessToken: accessToken, refreshToken: refreshToken)
            return token
        }
        return nil
    }
    
    func saveTokenToKeychain(token: Token) {
        let keychainItemWrapper = KeychainItemWrapper()
        keychainItemWrapper[Constant.access.rawValue] = token.accessToken as AnyObject
        keychainItemWrapper[Constant.refresh.rawValue] = token.refreshToken as AnyObject
    }
    
    func tokenExists() -> Bool {
        return getTokenFromKeychain != nil
    }
    
}
