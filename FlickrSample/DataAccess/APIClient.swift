//
//  APIClient.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Alamofire

protocol APIClientType {
    func request<T>(_: URLRequestConvertible, result: @escaping (Result<T, ClientError>)->(Void)) where T : Decodable, T : Encodable
}

class APIClient: APIClientType, Service {
    
    // MARK: Properties
    private let sessionManager: Session
    private let decoder: JSONDecoder
    
    // MARK: Lifecycle
    required init() {
        let configuration = AF
        configuration.sessionConfiguration.timeoutIntervalForRequest = 120
        configuration.sessionConfiguration.timeoutIntervalForResource = 120
        
        sessionManager = configuration
        decoder = JSONDecoder()
    }
    
    func request<T: Codable>(_ endpoint: URLRequestConvertible, result: @escaping (Result<T, ClientError>) -> (Void)) {
        guard let urlRequest = try? endpoint.asURLRequest() else {
            result(.failure(ClientError.invalidRequest))
            return
        }
        
        let request = self.sessionManager.request(urlRequest)
        
        #if DEBUG
        print("REQUEST: \(debugPrint(request))")
        #endif
        
        request
            .validate()
            .responseDecodable(of: T.self) { response in
                
                #if DEBUG
                print("Response: \(debugPrint(response))")
                #endif
                
            switch response.result {
            case .success(let model):
                result(.success(model))
            case .failure(let error):
                result(.failure(ClientError(rawValue: error.responseCode ?? 1002) ?? ClientError.unknown))
            }
        }
    }
    
    func cancelRequest() {
        sessionManager.cancelAllRequests()
    }
}
