//
//  AppDelegate.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private let clinet = APIClient()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupDependencies()
        
        changeFlow(to: .main)
        
        return true
    }

    func setupDependencies() {
        DependencyContainer.register(clinet)
    }

}


