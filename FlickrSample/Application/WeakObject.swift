//
//  WeakObject.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation

class WeakObject: Equatable {
    weak var value: AnyObject?
    
    init(value: AnyObject) {
        self.value = value
    }
    
    static func == (lhs: WeakObject, rhs: WeakObject) -> Bool {
        return lhs.value === rhs.value
    }
}
