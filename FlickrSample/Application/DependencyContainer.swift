//
//  DependencyContainer.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import Foundation

protocol Service: class {
    init()
}

final class DependencyContainer {
    private var dependencies: [String: WeakObject]
    static var shared = DependencyContainer()
    
    public static func register<T>(_ dependency: T) {
        return shared.register(dependency)
    }
    
    public static func resolve<T>() -> T {
        return shared.resolve()
    }
    
    private func register<T>(_ dependency: T) {
        let key = "\(type(of: T.self))"
        let weakObject = WeakObject(value: dependency as AnyObject)
        dependencies[key] = weakObject
    }
    
    private func resolve<T>() -> T {
        let key = "\(type(of: T.self))"

        guard let weakObject = dependencies[key] else {
            preconditionFailure("No dependency found for - \(key), Application must register a dependency befor resolving it.")
        }

        guard let dependency = weakObject.value as? T else {
            preconditionFailure("No dependency found for - \(key), Dependency is alredy deallocated by the system.")
        }

        return dependency
    }

    private init() {
        self.dependencies = [String: WeakObject]()
    }
}


