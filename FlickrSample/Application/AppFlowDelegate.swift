//
//  AppFlowDelegate.swift
//  FlickrSample
//
//  Created by J.arji on 12/20/20.
//

import UIKit

enum AppFlow {
    case main
    
    var viewController: UIViewController {
        switch self {
        case .main:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            return viewController

       
        }
    }
}

protocol AppFlowDelegate {
    func changeFlow(to flow: AppFlow)
}

extension AppFlowDelegate where Self: AppDelegate {
    private var transition: CATransition {
        let transition = CATransition()
        transition.type = .reveal
        transition.duration = 0.5
        
        return transition
    }
    
    func changeFlow(to flow: AppFlow) {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.layer.add(transition, forKey: kCATransition)
        self.window!.rootViewController = flow.viewController
        self.window!.makeKeyAndVisible()
    }
}

extension AppDelegate: AppFlowDelegate {}
